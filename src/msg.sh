#!/usr/bin/env bash
## [@bashly-upgrade]

## Print log messages to standard output or standard error, optionaly also to a log file
## You can disable colors and bold text with a NO_COLOR environment variable.
## Debug messages are sent only when the DEBUG environment variable is not emtpy.
## The messages are appended to a file if the LOG_FILE environment variable is set.
##
##      $ success All tasks finished well
##      [14:35] [SUCCESS] All tasks finished well
##
##      $ error Command exited with 1, aborting.
##      [14:35] [ERROR] Command exited with 1, aborting.
##

_timestamp() {
    bold "[$(date +"%H:%M")]"
}

_maybe_write_to_log_file() {
    NO_COLOR=1 _echo_message "$@" >>"${LOG_FILE:-/dev/null}"
}

success() {
    _echo_message() (
        echo "$(_timestamp) $(green_bold \[SUCCESS])" "$@"
    )

    _echo_message "$@"
    _maybe_write_to_log_file "$@"
}

info() {
    _echo_message() (
        echo "$(_timestamp) $(bold \[INFO])" "$@"
    )

    _echo_message "$@"
    _maybe_write_to_log_file "$@"
}

debug() {
    if [[ -n "${DEBUG:-}" ]]; then
        _echo_message() (
            echo "$(_timestamp) $(bold \[DEBUG])" "$@"
        )

        _echo_message "$@" >&2
        _maybe_write_to_log_file "$@"
    fi
}

warning() {
    _echo_message() (
        echo "$(_timestamp) $(yellow_bold \[WARNING])" "$@"
    )

    _echo_message "$@" >&2
    _maybe_write_to_log_file "$@"
}

error() {
    _echo_message() (
        echo "$(_timestamp) $(red_bold \[ERROR])" "$@"
    )

    _echo_message "$@" >&2
    _maybe_write_to_log_file "$@"
}
