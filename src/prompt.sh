## [@bashly-upgrade]

## Ask user for confirmation. You can pass custom messages.
##
##      $ prompt_confirmation
##      Continue? (y/n)
##
##      $ ask_to_continue
##		  Press any key to continue, CTRL+C to stop

function _prompt_helper() {
    read -n 1 -s -r -p "$*" ret
    echo "$ret"
}

## Echoes message to terminal and reads one character from stdin. Returns 0 if 'y' is read,
## returns 1 if 'n' is read. Repeats the message if any other character is read.
function prompt_confirmation() {
    local choice
    local ret
    local msg

    if [[ "$#" -eq 0 ]]; then
        msg="Continue? (y/n)"
    else
        msg="$*"
    fi

    choice=""

    while [[ ! "$choice" =~ [yn] ]]; do
        choice=$(_prompt_helper "$msg")
        echo
        case "$choice" in
        y) return 0 ;;
        n) return 1 ;;
        *) ;;
        esac
    done
}

## Prompts message to terminal, continues on any key press.
function ask_to_continue() {
    if [[ "$#" -eq 0 ]]; then
        _=$(_prompt_helper "Press any key to continue, CTRL+C to stop")
    else
        _=$(_prompt_helper "$*")
    fi
    echo
}
