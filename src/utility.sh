## [@bashly-upgrade]

## Utility functions I sometimes need. Accepts one argument with a path to file, or
## input on standard input.

strip_comments() {
    if [[ $# -eq 0 ]]; then
        sed 's/#.*$//g'
    else
        sed 's/#.*$//g' "$1"
    fi
}

escape_slash() {
    if [[ $# -eq 0 ]]; then
        sed 's/\//\\\//g'
    else
        sed 's/\//\\\//g' "$1"
    fi
}
