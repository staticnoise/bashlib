SHELL := /bin/bash
.PHONY: lint fix

lint:
	shfmt --language-dialect bash -i 4 -d src/*.sh
	find src -name '*.sh' -exec shellcheck -s bash {} \;

fix:
	shfmt --language-dialect bash -i 4 -w src/*.sh
