# My bash library

Bash functions I wrote and use in my scripts.

## How to use

Intended to use with [Bashly](https://bashly.dannyb.co):

```bash
# add all scripts
bashly add --source git:https://gitlab.com/staticnoise/bashlib.git all
```

**Can I use it without Bashly?**

Sure, but make sure to include all scripts required for the library to work.

## Changelog

### 2023-12-22

Changed msg.sh to support log files and debug messages with DEBUG and LOG_FILE environment variables.

## Copyright

Some of the scripts were originally written by [Danny Ben Shitrit](https://github.com/DannyBen) for [Bashly](https://bashly.dannyb.co).

```
The MIT License (MIT)

Copyright (c) Adam Chovanec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```
